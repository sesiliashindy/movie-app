import { useHistory } from 'react-router-dom'
import '../../assets/css/list.css'

const ListActor = (props) => {
    const history = useHistory()
    const {actor_id,name,img,desc} = props
    const viewDetail = (e) => {
        history.push({
            pathname: '/film',
            state:{
                actor_id:actor_id,
                key:name
            }
        })
    } 
    return(
        <div className="actor-list">
            <div className="list-box" key={name}>
                <div className="d-flex">
                    <div className="img-box" onClick={()=> viewDetail()}>
                        <img src={img} alt={img}/>
                    </div>   
                    <div className="box-container">
                        <h3>{name}</h3>
                        <h4>{desc}</h4>
                    </div>  
                </div>
            </div>
        </div>
    )
}
export default ListActor