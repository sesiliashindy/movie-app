import axios from 'axios'
import { useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import "../../../assets/css/list.css"
const EditList = (props) => {
    const history = useHistory()
    const {name,img,actor_id,desc,first_name,last_name} = props
    const viewDetail = (e) => {
        history.push({
            pathname: '/film',
            state:{
                actor_id:actor_id,
                key:name
            }
        })
    } 

    const goEdit = (e) => {
        history.push({
            pathname:'/edit',
            state: {
                firstName:first_name,
                lastName:last_name,
                actor_id:actor_id,
                key:name
            }
        })
    }

    const deleteActor = (e) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
          }).then((result) => {
            if (result.isConfirmed) {
                axios.delete(`http://localhost:8080/v1/api/dashboard/actor/delete/${actor_id}`)
                .then(res=>{
                    // Swal.fire(
                    //     'Deleted!',
                    //     'Your file has been deleted.',
                    //     'success'
                    //   )
                    window.location.reload()
                })
                .catch(error=>{
                    Swal.fire({
                        icon: 'error',
                        title: 'Delete Fail',
                        text: "Can't Delete This Actor!",
                    })
                    console.log(error)
                })
            }
          })
    }

    return(
        <div className="list-box" key={name}>
            <div className="d-flex">
            <div className="img-box" onClick={()=> viewDetail()}>
                <img src={img} alt={img}/>
            </div>   
            <div className="box-container">
                <h3>{name}</h3>
                <h4>{desc}</h4>
            </div>
            <div className="btn-box">
                <button type="button" className="btn btn-edit" onClick={()=> goEdit()}>Edit</button>
                <button type="button" className="btn btn-delete" onClick={()=> deleteActor()}>Delete</button>
            </div>
        </div>
        </div>
    )
}
export default EditList