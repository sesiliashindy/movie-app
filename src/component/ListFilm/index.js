import "../../assets/css/actor.css"
const ListFilm = (props) => {
    return(
        <div className="list-box" key={props.title}>
            <div className="d-flex">
            <div className="img-box-film">
                <img src={props.img} alt={props.img}/>
            </div>   
            <div className="box-container">
                <h3>{props.title}</h3>
                <h4>{props.desc}</h4>
                <h5>{props.date}</h5>
                <h5>{props.language}</h5>
            </div>
        </div>
        </div>
    )
}
export default ListFilm