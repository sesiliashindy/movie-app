import axios from "axios"
import { useState } from "react"
import { useHistory } from "react-router-dom"

const AddActor = () => {
    const history = useHistory()
    const [first_name,setFirstName] = useState('')
    const [last_name, setLastName] = useState('')

    const insertActor = (e) => {
        axios.post('http://localhost:8080/v1/api/dashboard/actor/add',{first_name,last_name})
        .then (
            history.push("/actor")
        ).catch(error=>{
            console.log(error)
        })
    } 

    return(
        <div className="list-box-edit" >
            <form className="add-film" id="form-field">
                <div className="mb-3">
                    <label for="exampleFormControlInput1" className="form-label">First Name</label>
                    <input type="text" required="required" name="first" className="form-control" placeholder="Actor's First Name" value={first_name} onChange={e=>setFirstName(e.target.value)}/>
                </div>
                <div className="mb-3">
                    <label for="exampleFormControlTextarea1" className="form-label">Last Name</label>
                    <input type="text" required="required" name="last" className="form-control" placeholder="Actor's Last Name" value={last_name} onChange={e=>setLastName(e.target.value)}/>
                </div>
                <button onClick={()=>insertActor()} className="btn btn-edit">Add</button>
            </form>
        </div>
    )
}
export default AddActor