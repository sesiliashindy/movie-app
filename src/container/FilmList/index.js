import axios from "axios";
import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import "../../assets/css/actor.css";
import { Movie } from "../../assets/img";
import ListFilm from "../../component/ListFilm";

const FilmList = (props) => {
    const{actor_id} = props.location.state
    const[listFilm,setListFilm] = useState([]) 
    const [pageNumber, setPageNumber] = useState('')

    const filmPerpage = 5
    const pageVisited = pageNumber * filmPerpage

    const pageCount = Math.ceil(listFilm.length/filmPerpage)

    const changePage = ({selected}) => {
        setPageNumber(selected)
    }

    const getListFilm = () =>{
        axios.get(`http://localhost:8080/v1/api/dashboard/film/${actor_id}`)
        .then(res => {
            setListFilm(res.data)
            // console.log(res.data)
        })
        .catch(err => console.log(err))
    }

    useEffect(() => {
        console.log(actor_id)
        getListFilm()
    }, [])
    return(
        <div className="">
            <div className="list-film">
                {listFilm.slice(pageVisited,pageVisited+filmPerpage)
                .map(element=>{
                    return(
                        <ListFilm
                            img = {Movie}
                            title = {element.title}
                            desc = {element.description}
                            date = {element.release_year}
                            language = {element.name}
                        />
                    )
                })}

                <ReactPaginate 
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    pageCount={pageCount}
                    onPageChange={changePage}
                    containerClassName={"paginationBtn"}
                    previousLinkClassName={"previousBttn"}
                    nextLinkClassName={"nextBttn"}
                    disabledClassName={"paginationDisabled"}
                    activeClassName={"paginationActive"}
                />
            </div>
        </div>
    )
}
export default FilmList