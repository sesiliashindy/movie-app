import axios from "axios";
import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import "../../assets/css/actor.css";
import { Default, Icon } from "../../assets/img";
import EditList from "../../component/ListActor/EditList";

const Actor = () => {
    const[listActor,setListActor] = useState([])
    const [pageNumber, setPageNumber] = useState('')

    const actorsPerpage = 5
    const pageVisited = pageNumber * actorsPerpage

    const pageCount = Math.ceil(listActor.length/actorsPerpage)

    const changePage = ({selected}) => {
        setPageNumber(selected)
    }

    const getActor = () => {
        axios.get("http://localhost:8080/v1/api/dashboard/actor/all")
        .then(res =>{
            setListActor(res.data)
            console.log(res.data)
        })
        .catch(err => 
            console.log(err))
    }

    useEffect(() => {
        getActor()
        // console.log(listActor)
    },[])
        
    return(
        <div className="actor-box">
            <img src={Icon} alt={Icon} className="icon"/>
        <div className="list-box-actor">
            <h2>Hello Admin!</h2>
            <h4>It's good to see you again</h4>
        </div>
        <div className="list-box-user">
            <h2>List Actors</h2>
            <h4>You can manages the actor's data here.^^</h4>
        </div>
            <div className="list-film">
                {listActor.slice(pageVisited,pageVisited+actorsPerpage).
                map(element=>{
                    return(
                        <EditList
                            img = {Default}
                            name = {element.first_name +" "+ element.last_name}
                            key = {element.actor_id}
                            actor_id = {element.actor_id}
                            first_name = {element.first_name}
                            last_name = {element.last_name}
                        />
                    )
                })}
                <ReactPaginate
                    previousLabel={"Previous"}
                    nextLabel={"Next"}
                    pageCount={pageCount}
                    onPageChange={changePage}
                    containerClassName={"paginationBtn"}
                    previousLinkClassName={"previousBttn"}
                    nextLinkClassName={"nextBttn"}
                    disabledClassName={"paginationDisabled"}
                    activeClassName={"paginationActive"}
                />
            </div>
        </div>
    )
}
export default Actor