import axios from "axios"
import { useState } from "react"
import { useHistory } from "react-router-dom"
import "../../assets/css/add.css"

const EditActor = (props) => {
    const history = useHistory()
    const {actor_id,firstName,lastName} = props.location.state
    const [first_name,setInputFirst] = useState('')
    const [last_name,setInputLast] = useState('')
    const updateActor = (e) => {
        axios.put('http://localhost:8080/v1/api/dashboard/actor/update',{actor_id,first_name,last_name})
        .then(res=>{
            history.push("/actor")
            window.location.reload()
        }).catch(error=>{
            console.log(error)
        })
    }

    // useEffect(() => {
    //     console.log(actor_id)
    // }, [])
    
    return(
        <div className="list-box-edit" >
            <form className="add-film" id="form-field">
                <div className="mb-3">
                    <label for="exampleFormControlInput1" className="form-label">First Name</label>
                    <input type="text" required="required" name="first" className="form-control" placeholder={firstName} value={first_name} onChange={e=>setInputFirst(e.target.value)}/>
                </div>
                <div className="mb-3">
                    <label for="exampleFormControlTextarea1" className="form-label">Last Name</label>
                    <input type="text" required="required" name="last" className="form-control" placeholder={lastName} value={last_name} onChange={e=>setInputLast(e.target.value)}/>
                </div>
                <button onClick={()=>updateActor()} className="btn btn-edit">Edit</button>
            </form>
        </div>
    )
}
export default EditActor