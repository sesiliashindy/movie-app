import Raya from "../img/raya.jpg"
import Godzila from "../img/godzila.jpg"
import Vivo from "../img/vivo.jpg"
import Icon from "../img/2.png"
import Default from "../img/default.png"
import Movie from "../img/movie.png"


export {
    Raya,
    Godzila,
    Vivo,
    Icon,
    Default,
    Movie
}