import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Actor from './container/Actor';
import ActorList from './container/ActorList';
import AddActor from './container/AddActor';
import EditActor from './container/EditActor';
import FilmList from './container/FilmList';

function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route path='/film' exact component={FilmList}/>
      <Route path='/edit-actor' exact component={Actor}/>
      <Route path='/edit' exact component={EditActor}/>
      <Route path='/add' exact component={AddActor}/>
      <Route path='/actor' exact component={ActorList}/>
    </Switch>
    </BrowserRouter>
  );
}

export default App;
